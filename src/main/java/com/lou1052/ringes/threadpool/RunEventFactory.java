package com.lou1052.ringes.threadpool;

import com.lmax.disruptor.EventFactory;

/**
 * Created with IntelliJ IDEA.
 * User: liuqing
 * Date: 2016/7/15
 * Time: 23:29
 * To change this template use File | Settings | File Templates.
 */
class RunEventFactory implements EventFactory<RunEvent> {
    @Override
    public RunEvent newInstance() {
        return new RunEvent();
    }
}
