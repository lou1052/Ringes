package com.lou1052.ringes.threadpool;

/**
 * 直接执行器  run by call
 * Created with IntelliJ IDEA.
 * User: liuqing
 * Date: 2016/7/18
 * Time: 22:36
 * To change this template use File | Settings | File Templates.
 */
 class DirectActuator implements Actuator{
    @Override
    public void onEvent(RunEvent runEvent, long l, boolean b) throws Exception {
        execute(runEvent.getRunnable());
    }

    @Override
    public void execute(Runnable command) {
        try {
            command.run();
        }catch (Exception e){

        }
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void onEvent(RunEvent runEvent) throws Exception {
        execute(runEvent.getRunnable());
    }
}
