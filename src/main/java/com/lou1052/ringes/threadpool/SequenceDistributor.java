package com.lou1052.ringes.threadpool;

import com.lmax.disruptor.dsl.Disruptor;

/**
 * 顺序选择器 根据序号
 * Created by liuqing-notebook on 2016/7/20.
 */
public class SequenceDistributor implements Distributor{
    @Override
    public Disruptor<RunEvent> selectOne(Disruptor<RunEvent>[] disruptors, int splitSize, int splitFactor, long sequence, boolean endOfBatch) {
        return disruptors[(int)(sequence&splitFactor)];
    }
}
