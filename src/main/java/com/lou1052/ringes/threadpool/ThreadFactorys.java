package com.lou1052.ringes.threadpool;

import java.util.concurrent.ThreadFactory;

/**
 * 统一管理线程工厂
 * Created by liuqing-notebook on 2016/7/20.
 */
class ThreadFactorys {
    static final ThreadFactory DAEMON_THREAD_FACTORY = (r)-> {
            Thread t = new Thread(r);
            t.setName("threadpool.daemon");
            t.setDaemon(true);
            return t;
        };

}
