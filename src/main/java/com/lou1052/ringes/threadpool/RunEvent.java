package com.lou1052.ringes.threadpool;

/**
 * Created with IntelliJ IDEA.
 * User: liuqing
 * Date: 2016/7/15
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */
class RunEvent {

    private Runnable runnable;

    /**
     * Getter for property 'runnable'.
     *
     * @return Value for property 'runnable'.
     */
    public Runnable getRunnable() {
        return runnable;
    }

    /**
     * Setter for property 'runnable'.
     *
     * @param runnable Value to set for property 'runnable'.
     */
    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }
}
