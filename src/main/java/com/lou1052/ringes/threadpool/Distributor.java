package com.lou1052.ringes.threadpool;

import com.lmax.disruptor.dsl.Disruptor;

/**
 * 任务分配器
 * Created by liuqing-notebook on 2016/7/18.
 */
public interface Distributor {
    Disruptor<RunEvent> selectOne(Disruptor<RunEvent>[] disruptors, int splitSize, int splitFactor, long sequence, boolean endOfBatch);
}
