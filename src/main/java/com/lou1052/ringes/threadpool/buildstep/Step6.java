package com.lou1052.ringes.threadpool.buildstep;

public interface Step6{
    Step7 runWhithDirectActuator();
    Step7 runWhithSynchronousActuator(String name, int size, boolean fair);
}
