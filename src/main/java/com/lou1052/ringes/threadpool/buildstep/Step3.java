package com.lou1052.ringes.threadpool.buildstep;

public interface Step3{
    Step4 directConnect();
    Step4 channelConnect(int bufferSize);
}
