package com.lou1052.ringes.threadpool.buildstep;

import com.lou1052.ringes.threadpool.Connector;

public interface Step8{
    Connector build();
}
