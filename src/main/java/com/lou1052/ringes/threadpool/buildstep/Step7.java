package com.lou1052.ringes.threadpool.buildstep;

import com.lou1052.ringes.threadpool.Connector;

public interface Step7{
    Step8  usePool(int poolSize);
    Connector build();
}
