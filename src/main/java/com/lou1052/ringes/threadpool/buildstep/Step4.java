package com.lou1052.ringes.threadpool.buildstep;

public interface Step4{
    Step5 connectAcceptor(String name, int bufferSize, int splitSize);
}
