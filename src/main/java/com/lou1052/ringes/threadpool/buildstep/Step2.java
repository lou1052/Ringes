package com.lou1052.ringes.threadpool.buildstep;

public interface Step2{
    Step3 isSingleProducer();
    Step3 isMultipleProducer();
    Step3 isSingleProducer(boolean isSingleProducer);
}
