package com.lou1052.ringes.threadpool;

/**
 * Created by liuqing-notebook on 2016/7/21.
 */
abstract class AbsConnector implements Connector{
   protected abstract int getRemainingCapacity();
}
