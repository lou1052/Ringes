package com.lou1052.ringes.threadpool;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;

import java.util.concurrent.Executor;

/**
 * 任务执行器
 * Created by liuqing-notebook on 2016/7/18.
 */
interface Actuator extends EventHandler<RunEvent> ,WorkHandler<RunEvent>, Executor {
     void shutdown();
}
