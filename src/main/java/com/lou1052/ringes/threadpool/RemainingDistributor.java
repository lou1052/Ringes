package com.lou1052.ringes.threadpool;

import com.lmax.disruptor.dsl.Disruptor;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * 空闲选择器 根据disruptors未完任务数 选择最小的
 * Created by liuqing-notebook on 2016/7/20.
 */
public class RemainingDistributor implements Distributor{

    private static final Comparator<Disruptor> findMaxRemainingCapacity = (a,b)->(int)(a.getRingBuffer().remainingCapacity()-b.getRingBuffer().remainingCapacity());

    @Override
    public Disruptor<RunEvent> selectOne(Disruptor<RunEvent>[] disruptors, int splitSize, int splitFactor, long sequence, boolean endOfBatch) {
        return Stream.of(disruptors).max(findMaxRemainingCapacity).get();
    }
}
