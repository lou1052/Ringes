package com.lou1052.ringes.threadpool;

/**
 * 任务连接器
 * Created by liuqing-notebook on 2016/7/18.
 */
public interface Connector {

     Connector connect(Acceptor acceptor);

     void transfer(Runnable command);

     void shutdown();

}
