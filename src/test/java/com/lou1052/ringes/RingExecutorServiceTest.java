package com.lou1052.ringes;

import com.lou1052.ringes.threadpool.ConnectorBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: liuqing
 * Date: 2016/7/21
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnit4.class)//测试PC win10 64 jdk1.8 cpu i7 3.60 GHz
public class RingExecutorServiceTest {

    @Test //448 398 413 433 383
    public void testDefault() throws Exception {
        ExecutorService ringExecutorService = new RingExecutorService();
        callTestCode1(ringExecutorService);
        ringExecutorService.shutdown();
    }

    @Test //394  334  338 356 390
    public void testSingleProducer() throws Exception {
        ExecutorService ringExecutorService = new RingExecutorService(true,65536,8,8192);
        callTestCode1(ringExecutorService);
        ringExecutorService.shutdown();
    }

    @Test //276 284 275 296 274
    public void testFast() throws Exception {
        ExecutorService ringExecutorService = new RingExecutorService(
                ConnectorBuilder.newBuilder()
                        .createConnector("connector")
                        .isSingleProducer()
                        .directConnect().connectAcceptor("acceptor",65536,8)
                        .sequenceDistributor()
                        .runWhithDirectActuator().build()
        );
        callTestCode1(ringExecutorService);
        ringExecutorService.shutdown();
    }

    @Test //275 285 277 279 296
    public void testFastUsePool() throws Exception {
        ExecutorService ringExecutorService = new RingExecutorService(
                ConnectorBuilder.newBuilder()
                        .createConnector("connector")
                        .isSingleProducer()
                        .directConnect().connectAcceptor("acceptor",65536,4)
                        .sequenceDistributor()
                        .runWhithDirectActuator().usePool(2)
                        .build()
        );
        callTestCode1(ringExecutorService);
        ringExecutorService.shutdown();
    }



    @Test //726  730  740  729 726
    public void testJdk1() throws Exception {
        ExecutorService jdkExecutorService = Executors.newFixedThreadPool(8);
        callTestCode1(jdkExecutorService);
        Thread.sleep(1000L);
        jdkExecutorService.shutdown();
    }

    @Test //330  347  366  416 398
    public void testJdk2() throws Exception {
        ExecutorService jdkExecutorService = Executors.newWorkStealingPool(8);
        callTestCode1(jdkExecutorService);
        Thread.sleep(1000L);
        jdkExecutorService.shutdown();
    }

    @Test //264 260 259 272 275
    public void testCall(){
        final long time= System.currentTimeMillis();
        for(int i=1;i<999999;i++) {
            final String idx = String.valueOf(i);
            final int count = i;
                String aa = "idx="+idx+"currentThread=" + Thread.currentThread() + " costtime=" + (System.currentTimeMillis() - time);
                int b = aa.length();
                if(count==999998){
                    System.out.println(b+"idx="+idx+"currentThread=" + Thread.currentThread() + " costtime=" + (System.currentTimeMillis() - time));
                }
        }
    }


    public void callTestCode1(ExecutorService es){
        final long time= System.currentTimeMillis();
        for(int i=1;i<999999;i++) {
            final String idx = String.valueOf(i);
            final int count = i;
            es.execute(() -> {
                String aa = "idx="+idx+"currentThread=" + Thread.currentThread() + " costtime=" + (System.currentTimeMillis() - time);
                int b = aa.length();
                if(count==999998){
                    System.out.println(b+"idx="+idx+"currentThread=" + Thread.currentThread() + " costtime=" + (System.currentTimeMillis() - time));
                }
            });
        }
    }


}
